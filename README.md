# Automated UI Test

This repository contains UI automated tests resolved with WebdriverIO framework. 

## Installation

```bash
npm i
   ```

## Running Tests


```bash
npm run wdio
```

## Test Results
The test results can be found in the console every execution. 



