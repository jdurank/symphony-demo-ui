

import Page from './page.js';

class HomePage extends Page {

  get displayedItems() {
    return $$('.inventory_item_name');
  }
  get sortBtn() {
    return $('.product_sort_container');
  }

  open() {
    return super.open();
  }

  returnItems() {
    const items = this.displayedItems.map((e) => e.getText());
    return items;

  }

  changeSortZA() {
    this.sortBtn.selectByVisibleText('Name (Z to A)');

  }

}

export default new HomePage();
