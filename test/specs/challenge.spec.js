
import LoginPage from '../pageobjects/login.page.js';
import homePage from '../pageobjects/home.page.js';

describe('Symphony Solutions challenge test', () => {

    beforeEach('Login to the App', async () => {
        await LoginPage.open();
        await LoginPage.login('standard_user', 'secret_sauce');
    });

    it('Should validate items are ordered from a-z.', async () => {
        // Verify that the items are sorted by Name (A -> Z)
        let origItemsArray = await homePage.returnItems();
        let ordItemsArray = await origItemsArray.sort();
        await expect(ordItemsArray).toEqual(origItemsArray);
    })

    it('Should order items from z-a and verify.', async () => {
        // Change the sorting to Name (Z -> A)
        await homePage.changeSortZA();

        // Verify that the items are sorted correctly
        let zaItemsArray = await homePage.returnItems();
        let orZadItemsArray = await zaItemsArray.reverse();
        await expect(zaItemsArray).toEqual(orZadItemsArray);
    })

})


